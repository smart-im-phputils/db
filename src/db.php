<?php

/** @var \PDO $dbh */
global $dbh;

if (!function_exists('db_connect')) {
    /**
     * Connect to database and initializes $dbh
     */
    function db_connect() {
        global $dbh;

        if (empty($dbh)) {
            $type = env('DB_TYPE', 'mysql');

            if ($type === 'mysql') {
                $opt = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_EMULATE_PREPARES => FALSE,];
                $dbh = new PDO("mysql:host=" . env('DB_HOST', 'localhost') . ";dbname=" . env('DB_NAME') . ";charset=utf8mb4", env('DB_USERNAME'), env('DB_PASSWORD'), $opt);
            } elseif ($type === 'sqlite') {
                $dbh = new PDO("sqlite:" . env('DB_PATH'));
            }
        }
    }
}

if (!function_exists('db_prepare_exec')) {
    /**
     * Internal function to prepare and execute query (returns $sth)
     */
    function db_prepare_exec($query, $args, &$rv = NULL) {
        global $dbh;

        $args = !empty($args[0]) && is_array($args[0]) ? $args[0] : $args;

        if (($fn = env('DB_DEBUG')) && !preg_match('/^describe/i', $query)) {
            $sql_string = $query;
            $indexed = $args == array_values($args);

            foreach ($args as $k => $v) {
                if (is_object($v)) {
                    if ($v instanceof \DateTime) $v = $v->format('Y-m-d H:i:s');
                    else continue;
                } elseif (is_string($v)) $v = "'$v'";
                elseif ($v === NULL) $v = 'NULL';
                elseif (is_array($v)) $v = implode(',', $v);

                if ($indexed) {
                    $sql_string = preg_replace('/\?/', $v, $sql_string, 1);
                } else {
                    if ($k[0] != ':') $k = ':' . $k; //add leading colon if it was left out
                    $sql_string = str_replace($k, $v, $sql_string);
                }
            }

            if (function_exists('app_dir')) {
                write_file(app_dir($fn, FALSE), sprintf("%s: %s\n", mysql_date(), $sql_string), TRUE);
            }
        }

        $sth = $dbh->prepare($query);
        $rv = $sth->execute($args);

        return $sth;
    }
}

if (!function_exists('db_exec')) {
    /**
     * Executes a insert/update query on database
     * Returns last_insert_id if insert, row count otherwise
     */
    function db_exec($query, ...$args) {
        global $dbh;

        $sth = db_prepare_exec($query, $args, $rv);

        return $rv ? (preg_match('/^insert\s+(ignore\s+)?into/i', $query) ? $dbh->lastInsertId() : $sth->rowCount()) : FALSE;
    }
}

if (!function_exists('db_query')) {
    /**
     * Executes a select query on database
     */
    function db_query($query, ...$args) {
        $sth = db_prepare_exec($query, $args);
        $results = $sth->fetchAll(PDO::FETCH_ASSOC);

        return $results;
    }
}

if (!function_exists('db_query_cached')) {
    /**
     * Executes a query on database and caches the results
     */
    function db_query_cached(int $expiry, string $query, ...$args) {
        $key = md5(json_encode(array_slice(func_get_args(), 1)));

        return cache_local($key, function () use ($query, $args) {
            return call_user_func_array('db_query', array_merge([$query], (array) $args));
        }, is_debug() ? 0 : $expiry);
    }
}

if (!function_exists('db_query_cached_single')) {
    /**
     * Executes a query on database, caches the results (in file) and returns the first row
     */
    function db_query_cached_single(int $expiry, string $query, ...$args) {
        $rows = call_user_func_array('db_query_cached', array_merge([$expiry, $query], $args));
        return !empty($rows) ? $rows[0] : NULL;
    }
}

if (!function_exists('db_query_single')) {
    /**
     * Executes a select query on database and returns the first rows
     */
    function db_query_single($query, ...$args) {
        if (preg_match('/^select/i', $query) && !preg_match('/limit\s+(\d+)/i', $query)) {
            $query = "$query limit 1";
        }

        $sth = db_prepare_exec($query, $args);
        $result = $sth->fetch(PDO::FETCH_ASSOC);
        $sth->closeCursor();

        return $result;
    }
}

if (!function_exists('db_query_value')) {
    /**
     * Executes a select query on database and returns the first column of first row
     * SELECT user_id from users limit 1; // returns user_id of first row
     */
    function db_query_value($query, ...$args) {
        $sth = db_prepare_exec($query, $args);
        $result = $sth->fetch(PDO::FETCH_NUM);
        $sth->closeCursor();

        return !empty($result) ? $result[0] : NULL;
    }
}

if (!function_exists('db_query_values')) {
    /**
     * Executes a select query on database and returns the first column of all rows as array
     * SELECT user_id from users where name = 'san'; // returns all user_id where name = 'san'
     */
    function db_query_values($query, ...$args) {
        $sth = db_prepare_exec($query, $args);
        $result = $sth->fetchAll(PDO::FETCH_COLUMN, 0);

        return !empty($result) ? $result : [];
    }
}

if (!function_exists('db_query_paginated')) {
    /**
     * Executes a select query on db and returns paginated results that can be rendered with vue2-paginated-results (in vue2-paginate)
     *
     */
    function db_query_paginated($query, ...$args) {
        $start = intval($_REQUEST['items_start'] ?? 0);
        $limit = intval($_REQUEST['items_num'] ?? 10);
        $total = intval($_REQUEST['items_total'] ?? 0);
        $order = $_REQUEST['items_order'] ?? NULL;

        if (empty($total)) {
            $sql = preg_replace('/^(select\s+)(.*?)(\s+from )/i', '$1 COUNT(*) $3', $query, 1);
            $total = call_user_func_array('db_query_value', array_merge([$sql], $args));
        }

        $sth = db_prepare_exec("$query " . ($order ? 'ORDER BY ?' : '') . " LIMIT ?, ?", array_merge($args, $order ? [$order] : [], [$start, $limit]));
        $results = $sth->fetchAll(PDO::FETCH_ASSOC);

        return ['results' => $results, '$meta' => ['items_start' => $start, 'items_num' => $limit, 'items_total' => $total, 'items_order' => $order]];
    }
}


if (!function_exists('db_table_schema')) {
    /**
     * Returns the table schema
     */
    function db_table_schema($table) {
        $rows = db_query_cached(86400, "DESCRIBE $table");

        foreach ($rows as $row) {
            $results['fields'][] = $field = $row['Field'];

            if ($row['Key'] === 'PRI') {
                $results['primary_key'] = !empty($results['primary_key']) ? array_merge((array) $results['primary_key'], $field) : $field;
            }

            $results['field'][$field] = trim(preg_replace('/\W.*/', '', $row['Type']));
        }

        return $results;
    }
}

if (!function_exists('db_list_tables')) {
    /**
     * Returns the list of tables in a database
     */
    function db_list_tables($cached = TRUE) {
        $results = db_query_cached(86400, 'show tables');

        return array_column(array_map('array_values', $results), 0);
    }
}

if (!function_exists('db_find')) {
    /**
     * Finds a record
     */
    function db_find($table, $matches = [], $limit = 1, $order = '') {
        $match_str = join(' and ', array_map(function ($k) { return "(`$k` <=> ?)"; }, array_keys($matches)));
        $query = sprintf('SELECT * FROM `%s` WHERE %s %s', $table, $match_str ?: 1, is_array($limit) ? sprintf("limit %d %d", $limit[0], $limit[1]) : ($limit > 0 ? "limit $limit" : ''));

        if (!empty($order)) {
            $query = "$query ORDER BY $order";
        }

        $results = call_user_func_array('db_query', array_merge([$query], array_values($matches)));

        return !empty($results) ? ($limit === 1 ? $results[0] : $results) : NULL;
    }
}

if (!function_exists('db_find_cached')) {
    /**
     * Finds a record using cache (local file system)
     */
    function db_find_cached(int $expiry, $table, $matches = [], $limit = 1) {
        $key = md5(json_encode(array_slice(func_get_args(), 1)));

        return cache_local($key, function () use ($table, $matches, $limit) {
            return call_user_func_array('db_find', [$table, $matches, $limit]);
        }, is_debug() ? 0 : $expiry);
    }
}

if (!function_exists('db_cache_result')) {
    /**
     * Finds a record using the `cache` table in current database (set refresh to true to force update cache)
     */
    function db_cache_result($name, $value = NULL, $category = NULL, $expires = '+1 day', $refresh = FALSE) {
        $key = md5($name); //length of name col is 32

        if (!$refresh) {
            if ($result = db_query_value('select value from cache where name = ? and expires_at >= NOW()', $key)) {
                $result = json_decode_array($result);
            }
        }

        if (empty($result)) {
            $result = $value instanceof \Closure ? $value() : $value;
            $values = ['name' => $key, 'value' => $result === NULL ? $result : json_encode($result), 'expires_at' => date('Y-m-d h:i:s', is_numeric($expires) ? time() + $expires : strtotime($expires)), 'category' => $category];
            db_insert('cache', $values, FALSE, 'replace');
        }

        return $result;
    }
}

if (!function_exists('db_cache_clear')) {
    /**
     * Clears all records belonging to a certain category
     */
    function db_cache_clear($category) {
        db_update('cache', ['category' => $category], ['expires_at' => '19700101'], FALSE, FALSE, 0);
    }
}

if (!function_exists('db_insert')) {
    /**
     * Inserts a row into a table optionally filling out fields like created_at, updated_at, etc
     */
    function db_insert($table, array $values, $magic = FALSE, $insert_ignore = FALSE) {
        if (!empty($magic)) {
            $schema = db_table_schema($table);

            foreach ($values as $key => $value) {
                if (!in_array_nc($key, $schema['fields'])) {
                    unset($values[$key]);
                }
            }

            if (in_array('user_id', $schema['fields'])) {
                $values['user_id'] = $values['user_id'] ?? user_id();
            }

            foreach (['created_at', 'updated_at'] as $field) {
                if (in_array($field, $schema['fields'])) {
                    $values[$field] = $values[$field] ?? mysql_date();
                }
            }
        }

        $mode = strcasecmp($insert_ignore, 'replace') === 0 ? 'replace' : ($insert_ignore === TRUE ? 'insert ignore' : 'insert');
        $query = sprintf('%s INTO `%s` (%s) VALUES (%s)', $mode, $table, join(', ', str_quote(array_keys($values), "`")), join(', ', array_fill(0, count(array_keys($values)), '?')));

        return call_user_func_array('db_exec', array_merge([$query], array_values(array_map(function ($f) { return $f === NULL ? NULL : (is_scalar($f) ? $f : json_encode($f)); }, $values))));
    }
}

if (!function_exists('db_insert_slug')) {
    /**
     * Inserts a row into a table making sure that value of slug_column is always unique (e.g. project-1, project-2 when project, project-1, exist)
     */
    function db_insert_slug($table, array $values, $slug_column = 'slug', $magic = FALSE, $insert_ignore = FALSE) {
        $oname = @$values[$slug_column] ?: $slug_column;
        $count = 0;

        do {
            $name = ++$count === 1 ? $oname : sprintf("%s-%s", $oname, $count);
            $exists = db_query_value(sprintf('SELECT COUNT(*) as total from %s where %s = ?', $table, $slug_column), $name);
        } while ($exists > 0);

        return db_insert($table, array_merge($values, [$slug_column => $name]), $magic, $insert_ignore);
    }
}

if (!function_exists('db_update')) {
    /**
     * Updates matching rows optionally filling out fields like updated_at, etc
     */
    function db_update($table, array $matches, array $updates, $magic = FALSE, $update_ignore = FALSE, $limit = 1) {
        if (!empty($magic)) {
            $schema = db_table_schema($table);

            foreach ($updates as $key => $value) {
                if (!in_array_nc($key, $schema['fields'])) {
                    unset($updates[$key]);
                }
            }

            if (in_array('updated_at', $schema['fields'])) {
                $updates['updated_at'] = $updates['updated_at'] ?? mysql_date();
            }
        }

        $update_str = join(', ', array_map(function ($k) { return "`$k` = ?"; }, array_keys($updates)));
        $match_str = join(' and ', array_map(function ($k) { return "(`$k` <=> ?)"; }, array_keys($matches)));
        $query = sprintf('UPDATE %s `%s` SET %s WHERE %s %s', $update_ignore ? 'ignore' : '', $table, $update_str, $match_str ?: 1, $limit > 0 ? "limit $limit" : '');

        return call_user_func_array('db_exec', array_merge([$query], array_values(array_map(function ($f) { return $f === NULL ? NULL : (is_scalar($f) ? $f : json_encode($f)); }, $updates)), array_values($matches)));
    }
}

if (!function_exists('db_count')) {
    /**
     * Returns the count of matching rows
     */
    function db_count($table, array $matches) {
        $match_str = join(' and ', array_map(function ($k) { return "(`$k` <=> ?)"; }, array_keys($matches)));
        $query = sprintf('SELECT COUNT(*) FROM `%s` WHERE %s', $table, $match_str ?: 1);

        return call_user_func_array('db_query_value', array_merge([$query], array_values($matches))) ?: 0;
    }
}

if (!function_exists('db_delete')) {
    /**
     * Delets matching rows
     */
    function db_delete($table, array $matches, $limit = 1) {
        $match_str = join(' and ', array_map(function ($k) { return "(`$k` <=> ?)"; }, array_keys($matches)));
        $query = sprintf('DELETE FROM `%s` WHERE %s %s', $table, $match_str ?: 1, $limit > 0 ? "limit $limit" : '');

        return call_user_func_array('db_exec', array_merge([$query], array_values($matches)));
    }
}

if (!function_exists('db_crud')) {
    /**
     * Easy way to do CRUD operation on database
     */
    function db_crud($op, $table, $match, $updates = NULL, $failsafe = ['id', 'user_id'], $json_fields = []) {
        if (!preg_match('/^(create|read|update|delete|list)$/', $op)) {
            abort('invalid operation');
        }

        if ($op === 'create') {
            if ($id = db_insert($table, array_merge($updates, $match), TRUE, TRUE)) {
                $match = ['id' => $id];
            }
        } elseif ($op === 'list' && !empty($match['user_id'])) {
            return db_find($table, $match, 0);
        } elseif ($failsafe && !call_user_func_array('has_keys', array_merge([$match], $failsafe))) {
            abort(sprintf('Invalid data for %s', join(', ', $failsafe)));
        }

        if ($row = db_find($table, $match)) {
            if (($op === 'read') || ($op === 'create')) {
                foreach ($json_fields as $jf) {
                    if (!empty($row[$jf])) {
                        $row[$jf] = json_decode_array($row[$jf]) ?: $row[$jf];
                    }
                }

                return $row;
            } elseif ($op === 'update') {
                return db_update($table, $match, $updates, TRUE, TRUE);
            } elseif ($op === 'delete') {
                return db_delete($table, $match) ?: FALSE;
            }
        }

        return FALSE;
    }
}

if (!function_exists('db_quote')) {
    /**
     * Quotes a string using PDO
     */
    function db_quote($str) {
        global $dbh;

        return $dbh->quote($str);
    }
}