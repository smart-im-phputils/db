## Installation

Just run `composer install [package_name]`

After that all these functions should be available in every PHP file (via composer's autoloader)

- [db_connect](#db_connect)

- [db_prepare_exec](#db_prepare_exec)

- [db_exec](#db_exec)

- [db_query](#db_query)

- [db_query_cached](#db_query_cached)

- [db_query_cached_single](#db_query_cached_single)

- [db_query_single](#db_query_single)

- [db_query_value](#db_query_value)

- [db_query_values](#db_query_values)

- [db_query_paginated](#db_query_paginated)

- [db_table_schema](#db_table_schema)

- [db_list_tables](#db_list_tables)

- [db_find](#db_find)

- [db_find_cached](#db_find_cached)

- [db_cache_result](#db_cache_result)

- [db_cache_clear](#db_cache_clear)

- [db_insert](#db_insert)

- [db_insert_slug](#db_insert_slug)

- [db_update](#db_update)

- [db_count](#db_count)

- [db_delete](#db_delete)

- [db_crud](#db_crud)

- [db_quote](#db_quote)

### db_connect

@var \PDO $dbh *

```php
function db_connect()
```


### db_prepare_exec

Internal function to prepare and execute query (returns $sth)

```php
function db_prepare_exec($query, $args, &$rv = NULL)
```


### db_exec

Executes a insert/update query on database
 Returns last_insert_id if insert, row count otherwise

```php
function db_exec($query, ...$args)
```


### db_query

Executes a select query on database

```php
function db_query($query, ...$args)
```


### db_query_cached

Executes a query on database and caches the results

```php
function db_query_cached(int $expiry, string $query, ...$args)
```


### db_query_cached_single

Executes a query on database, caches the results (in file) and returns the first row

```php
function db_query_cached_single(int $expiry, string $query, ...$args)
```


### db_query_single

Executes a select query on database and returns the first rows

```php
function db_query_single($query, ...$args)
```


### db_query_value

Executes a select query on database and returns the first column of first row
 SELECT user_id from users limit 1; // returns user_id of first row

```php
function db_query_value($query, ...$args)
```


### db_query_values

Executes a select query on database and returns the first column of all rows as array
 SELECT user_id from users where name = 'san'; // returns all user_id where name = 'san'

```php
function db_query_values($query, ...$args)
```


### db_query_paginated

Executes a select query on db and returns paginated results that can be rendered with vue2-paginated-results (in vue2-paginate)

```php
function db_query_paginated($query, ...$args)
```


### db_table_schema

Returns the table schema

```php
function db_table_schema($table)
```


### db_list_tables

Returns the list of tables in a database

```php
function db_list_tables($cached = TRUE)
```


### db_find

Finds a record

```php
function db_find($table, $matches = [], $limit = 1, $order = '')
```


### db_find_cached

Finds a record using cache (local file system)

```php
function db_find_cached(int $expiry, $table, $matches = [], $limit = 1)
```


### db_cache_result

Finds a record using the `cache` table in current database (set refresh to true to force update cache)

```php
function db_cache_result($name, $value = NULL, $category = NULL, $expires = '+1 day', $refresh = FALSE)
```


### db_cache_clear

Clears all records belonging to a certain category

```php
function db_cache_clear($category)
```


### db_insert

Inserts a row into a table optionally filling out fields like created_at, updated_at, etc

```php
function db_insert($table, array $values, $magic = FALSE, $insert_ignore = FALSE)
```


### db_insert_slug

Inserts a row into a table making sure that value of slug_column is always unique (e.g. project-1, project-2 when project, project-1, exist)

```php
function db_insert_slug($table, array $values, $slug_column = 'slug', $magic = FALSE, $insert_ignore = FALSE)
```


### db_update

Updates matching rows optionally filling out fields like updated_at, etc

```php
function db_update($table, array $matches, array $updates, $magic = FALSE, $update_ignore = FALSE, $limit = 1)
```


### db_count

Returns the count of matching rows

```php
function db_count($table, array $matches)
```


### db_delete

Delets matching rows

```php
function db_delete($table, array $matches, $limit = 1)
```


### db_crud

Easy way to do CRUD operation on database

```php
function db_crud($op, $table, $match, $updates = NULL, $failsafe = ['id', 'user_id'], $json_fields = [])
```


### db_quote

Quotes a string using PDO

```php
function db_quote($str)
```


